package com.t0rrent.alphahexmcmod;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.Gui;
import net.minecraft.client.gui.ScaledResolution;

public class GuiModOverlay extends Gui {
	public static long lastTime = System.nanoTime();

	public GuiModOverlay(Minecraft mc) {
		ScaledResolution scaled = new ScaledResolution(mc);
		int width = scaled.getScaledWidth();
		int height = scaled.getScaledHeight();
		if (mc.player.getHealth() <= (float) (AlphaHexMCmod.hearts * 2) && AlphaHexMCmod.modEnable) {
//			this.square(0, 2013200384, mc);
			this.square(0, 1727987712, mc);
			this.square(1, 1442775040, mc);
			this.square(2, 1157562368, mc);
			this.square(3, 872349696, mc);
			this.square(4, 587137024, mc);
//			this.square(6, 301924352, mc);
		}

	}

	private void square(int k, int j, Minecraft mc) {
		int i = k * 2;
		int l = 2;
		ScaledResolution scaled = new ScaledResolution(mc);
		int width = scaled.getScaledWidth();
		int height = scaled.getScaledHeight();
		drawRect(i, i, width - i, l + i, j);
		drawRect(i, height - (l + i), width - i, height - i, j);
		drawRect(i, height - (l + i), l + i, l + i, j);
		drawRect(width - i, height - (l + i), width - (l + i), l + i, j);
	}
}
