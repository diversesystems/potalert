package com.t0rrent.alphahexmcmod;

import net.minecraft.client.Minecraft;
import net.minecraft.init.SoundEvents;
import net.minecraftforge.client.event.RenderGameOverlayEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.gameevent.TickEvent;

public class EventHandler {

	public int tick = 0;

	public EventHandler() {

	}

	@SubscribeEvent
	public void onRenderGui(RenderGameOverlayEvent.Post event) {
		if (event != null && AlphaHexMCmod.mc.world != null) {
			if (event.getType() == RenderGameOverlayEvent.ElementType.ALL) {
				new GuiModOverlay(Minecraft.getMinecraft());
			}
		}
	}

	@SubscribeEvent
	public void onClientTick(TickEvent.ClientTickEvent e) {
		if (e != null && AlphaHexMCmod.mc.world != null) {

			if (AlphaHexMCmod.mc.player.getHealth() <= (float) (AlphaHexMCmod.hearts * 2) && AlphaHexMCmod.modEnable) {
				tick++;
				if(tick % 30 == 0)
					AlphaHexMCmod.mc.player.playSound(SoundEvents.ENTITY_ARROW_HIT_PLAYER, .1f,  .75f);
			}
			if(tick >= 30)
				tick = 0;
		}
	}
}
