package com.t0rrent.alphahexmcmod;

import net.minecraft.command.CommandBase;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommandSender;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.client.IClientCommand;


public class CommandPotalert extends CommandBase implements IClientCommand {
	public static final CommandPotalert INSTANCE = new CommandPotalert();

	public CommandPotalert() {
	}

	public String getName() {
		return "potalert";
	}

	public String getUsage(ICommandSender var1) {
		return "/potalert [number from 1 - 9/disable]";
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length == 0) {
			sender.sendMessage(new TextComponentString(TextFormatting.RED + "Invalid argument, please use /potalert [number from 1 - 9/disable]"));
		} else {
			if (!args[0].equals("disable")) {
				try {
					if (Integer.parseInt(args[0]) <= 0 || Integer.parseInt(args[0]) >= 10) {
						sender.sendMessage(new TextComponentString(TextFormatting.RED + "Invalid argument, please use /potalert [number from 1 -  9/disable]"));
						return;
					}
				} catch (NumberFormatException var4) {
					sender.sendMessage(new TextComponentString(TextFormatting.RED + "Invalid argument, please use /potalert [number from 1 - 9/disable]"));
					return;
				}
			}

			if (args[0].equals("disable")) {
				sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Disabled potalert"));
				AlphaHexMCmod.modEnable = false;
				try {
					AlphaHexMCmod.saveConfig();
				} catch (Exception e) {
					e.printStackTrace();
				}
			} else {
				AlphaHexMCmod.modEnable = true;
				sender.sendMessage(new TextComponentString(TextFormatting.GREEN + "Potalert set to " + Integer.parseInt(args[0])));
				AlphaHexMCmod.hearts = Integer.parseInt(args[0]);
				try {
					AlphaHexMCmod.saveConfig();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}


	public boolean checkPermission(MinecraftServer server, ICommandSender sender) {
		return true;
	}

	@Override
	public boolean allowUsageWithoutPrefix(ICommandSender sender, String message) {
		return false;
	}

}
