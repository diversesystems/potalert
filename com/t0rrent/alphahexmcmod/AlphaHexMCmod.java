package com.t0rrent.alphahexmcmod;

import net.minecraft.client.Minecraft;
import net.minecraftforge.client.ClientCommandHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import java.io.*;

@Mod(
		modid = "fuck off alphahex",
		name = "Pot Alert",
		version = "1.0"
)
public class AlphaHexMCmod {

	@Mod.Instance
	public static AlphaHexMCmod instance = new AlphaHexMCmod();

	public static Minecraft mc;
	private static File configFolder;
	private static File config;
	public static boolean modEnable;
	public static int hearts;

	@Mod.EventHandler
	public void init(FMLInitializationEvent event) throws Exception {
		mc = Minecraft.getMinecraft();

		StringBuilder sb = new StringBuilder();
		sb.append(mc.mcDataDir).append("//config//PotAlert");
		configFolder = new File(sb.toString());

		if (!configFolder.exists())
			configFolder.mkdirs();

		sb = new StringBuilder();
		sb.append(configFolder).append("//PotAlertConfig.txt");
		config = new File(sb.toString());

		if (!config.exists())
			config.createNewFile();

		readConfig();

		MinecraftForge.EVENT_BUS.register(this);
		FMLCommonHandler.instance().bus().register(this);
		MinecraftForge.EVENT_BUS.register(new EventHandler());

		ClientCommandHandler.instance.registerCommand(CommandPotalert.INSTANCE);

	}

	public static void readConfig() throws Exception {
		BufferedReader br = new BufferedReader(new FileReader(config));
		String temp = br.readLine();
		String[] split;

		if (temp == null) {
			fallbackConfig();
			return;
		}

		try {
			if (temp != null) {
				split = temp.split("\\s+");
				modEnable = Boolean.parseBoolean(split[1]);
			} else {
				throw new Exception("fallback");
			}

			temp = br.readLine();
			if (temp != null) {
				split = temp.split("\\s+");

				if (split.length > 1) {
					hearts = Integer.parseInt(split[1]);
				} else
					throw new Exception("fallback");
			}

		} catch (Exception e) {
			fallbackConfig();
		}
	}

	private static void fallbackConfig() {
		modEnable = true;
		hearts = 4;
	}

	public static void saveConfig() throws Exception {
		BufferedWriter bw = new BufferedWriter(new FileWriter(config));
		StringBuilder sb = new StringBuilder();

		sb.append("Enabled: ").append(Boolean.toString(modEnable)).append("\n");
		sb.append("Hearts_Alert: ").append(Integer.toString(hearts)).append("\n");

		bw.write(sb.toString());

		bw.close();
	}

}
